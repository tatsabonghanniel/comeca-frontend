import React, { useState, useEffect } from 'react';
import { TextInput, Text, ImageBackground, StyleSheet, Image, PixelRatio, Modal, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Container, Content, View, Button, Input, Form, Item, Icon } from 'native-base';
import StyledText from '../../components/StyledText';
import { useTheme } from '@react-navigation/native';
import { AuthProvider } from '../../providers/Auth';
import { useDispatch } from 'react-redux';
import { getCurrentUserAction } from '../../redux/actions/CurrentUser';

export default function LoginScreen({ navigation }) {
    const { colors } = useTheme();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [passwordVisible, setPasswordVisible] = useState(false);

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState({ enabled: false, message: '' });

    const dispatch = useDispatch();

    const ValidateEmail = (mail) => {
        return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail);
    }

    // Handle create account button press
    async function loginAccount() {
        if (!email || !password) {
            setError({ enabled: true, message: 'Veullez remplir tous les champs !' });
            return false;
        }

        if (!ValidateEmail(email)) {
            setError({ enabled: true, message: "Format d'adresse mail invalide !" });
            return false;
        }

        if (password.length < 6) {
            setError({ enabled: true, message: 'Le mot de passe doit comporter au moins 6 caractères !' });
            return false;
        }

        setLoading(true);

        AuthProvider.login(email, password)
            .then((result) => {
                console.log("Connect !");
                if (result.error) {
                    if (result.error == 'no-match') {
                        setError({ enabled: true, message: 'Combinaison identifiant/Password invalide !' });
                    }
                } else {
                    dispatch(getCurrentUserAction());
                }
            })
            .finally(() => {
                setLoading(false);
            })
    }


    return (
        <View style={{ flex: 1 }}>


            <Modal visible={loading} transparent>
                <View style={styles.modalContent}>
                    <View style={[{ backgroundColor: colors.primary }, styles.loadingModal]}>
                        <StyledText style={{ color: colors.specialText, marginVertical: 5 }}>Authentification</StyledText>
                        <ActivityIndicator size={30} color={colors.specialText} />
                    </View>
                </View>
            </Modal>

            <Modal visible={error.enabled} transparent onRequestClose={() => { setError({ enabled: false, message: '' }) }}>
                <View style={styles.modalContent}>
                    <View style={[{ backgroundColor: colors.warning }, styles.errorModal]}>
                        <TouchableOpacity
                            style={styles.modalCloseButton}
                            onPress={() => { setError({ enabled: false, message: '' }) }}
                        >
                            <StyledText style={{ color: colors.specialText }}>X</StyledText>
                        </TouchableOpacity>
                        <StyledText style={{ color: colors.specialText, marginVertical: 5 }}>{error.message}</StyledText>
                    </View>
                </View>
            </Modal>


            <View style={{ alignSelf: 'center', marginVertical: 20 }}>
                <StyledText style={{ color: colors.primary, fontSize: 20 }}>Connectez-vous à votre compte</StyledText>
            </View>

            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                    source={require('./../../assets/images/svg/2.png')}
                    style={{ height: '100%' }}
                    width={'60%'}
                />
            </View>

            <View style={{ flex: 2, marginHorizontal: 15, marginTop: 40 }}>
                <Form>
                    <Item style={{ borderBottomWidth: 0 }}>
                        <Icon type='MaterialIcons' name='person' style={{ position: 'absolute', left: 12, color: colors.primary }} />
                        <Input placeholderTextColor={colors.placeholder} placeholder="Adresse mail" style={{ color: colors.primary, fontWeight: 'bold', paddingLeft: 50, borderColor: colors.primary, borderWidth: .65 / PixelRatio.getPixelSizeForLayoutSize(1), borderRadius: 15, backgroundColor: colors.placeholderBg }}
                            onChangeText={setEmail}
                        />
                    </Item>
                    <Item style={{ borderBottomWidth: 0, marginVertical: 10 }} last>
                        <Icon type='MaterialIcons' name='lock' style={{ position: 'absolute', left: 28, color: colors.primary, fontSize: 23 }} />
                        <Input placeholderTextColor={colors.placeholder} textContentType='password' secureTextEntry={!passwordVisible} placeholder="Mot de passe" style={{ color: colors.primary, fontWeight: 'bold', paddingLeft: 50, borderColor: colors.primary, borderWidth: .65 / PixelRatio.getPixelSizeForLayoutSize(1), borderRadius: 15, backgroundColor: colors.placeholderBg }}
                            onChangeText={setPassword}
                        />
                        <TouchableOpacity
                            style={{ position: 'absolute', right: 10 }}
                            onPress={() => { setPasswordVisible(visible => !visible) }}
                        >
                            <Icon type='MaterialIcons' name={passwordVisible ? 'visibility-off' : 'visibility'} style={{ color: colors.primary, fontSize: 23 }} />
                        </TouchableOpacity>
                    </Item>
                </Form>

                <View style={{ marginTop: 8 }}>
                    <Button rounded style={[styles.button, { backgroundColor: colors.primary }]}
                        onPress={() => { loginAccount() }}
                    >
                        <StyledText style={[styles.buttonText, { color: colors.specialText }]}>Se connecter</StyledText>
                    </Button>
                    <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'center' }}>
                        <StyledText type='medium'>Pas encore membre ? </StyledText>
                        <TouchableOpacity
                            onPress={() => { navigation.navigate('RegisterScreen') }}
                        >
                            <StyledText style={{ color: colors.success }}>Inscrivez-vous</StyledText>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>

        </View>
    )

}


const styles = StyleSheet.create({
    modalContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(1, 1, 1, .5)'
    },
    loadingModal: {
        width: '50%',
        height: 100,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalCloseButton: {
        paddingHorizontal: 10,
        position: 'absolute',
        top: 7,
        right: 5
    },
    errorModal: {
        width: '70%',
        height: 90,
        paddingHorizontal: 15,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    header: {
        flex: 1,
        alignItems: 'center',
        marginVertical: 20
    },
    buttonsSection: {
        flex: 1,
        marginVertical: 20,
        justifyContent: 'flex-end'
    },
    button: {
        marginVertical: 6,
        height: 50,
        alignSelf: 'center',
    },
    buttonText: {
        width: '90%',
        textAlign: 'center',
    }
})