import React, { useEffect } from 'react';
import { ImageBackground, StyleSheet, ToastAndroid } from 'react-native';
import { View, Button } from 'native-base';
import StyledText from '../../components/StyledText';
import { useTheme } from '@react-navigation/native';

const AuthScreen = ({ navigation }) => {

    const { colors } = useTheme();

    useEffect(() => {

        return () => {

        }
    }, []);

    return (
        <ImageBackground source={require('./../../assets/images/svg/3.png')}
            style={styles.imageBackground}>
            <View style={styles.container}>
                <View style={styles.header}>
                    <StyledText type='bold' style={{ textAlign: 'center', fontSize: 33, color: colors.placeholder }}>Bienvenue sur TREE LOCATOR</StyledText>
                </View>
                <View style={styles.buttonsSection}>
                    <Button primary rounded style={[styles.button, { backgroundColor: colors.placeholder }]}
                        onPress={() => { navigation.navigate('LoginScreen') }}
                    >
                        <StyledText style={[styles.buttonText, { color: colors.specialText }]}>Se connecter</StyledText>
                    </Button>
                    <Button info rounded style={[styles.button, { backgroundColor: colors.primary }]}
                        onPress={() => { navigation.navigate('RegisterScreen') }}
                    >
                        <StyledText style={[styles.buttonText, { color: colors.specialText }]}>S'inscrire</StyledText>
                    </Button>
                </View>
            </View>
            {/* <Button title="Login" onPress={() => createAccount()} /> */}
        </ImageBackground>
    );

}


const styles = StyleSheet.create({
    imageBackground: {
        width: '100%',
        height: '95%'
    },
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    header: {
        flex: 1,
        alignItems: 'center',
        marginVertical: 20
    },
    buttonsSection: {
        flex: 1,
        marginVertical: 20,
        justifyContent: 'flex-end'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 6,
        alignSelf: 'center',
        height: 52
    },
    buttonText: {
        width: '80%',
        textAlign: 'center'
    }
})

export default AuthScreen;