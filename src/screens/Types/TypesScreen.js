import React from 'react'
import { useFocusEffect, useTheme } from '@react-navigation/native';
import { Body, Button, Container, Content, Fab, Header, Icon, Left, Right } from 'native-base';
import { View, BackHandler, StyleSheet, TouchableOpacity, Modal, ActivityIndicator } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import TypeListCard from '../../components/Types/TypeListCard';
import StyledText from '../../components/StyledText';
import InputModal from '../../helpers/InputModal';
import { useState } from 'react';
import { TypesArbresProvider } from '../../providers/TypesArbres';
import { getTypesArbresAction } from '../../redux/actions/TypesArbres';

const TypesScreen = ({ route, navigation }) => {

    const { colors } = useTheme();

    const [addingType, setAddingType] = useState(false);
    const [pending, setPending] = useState(false);

    const [error, setError] = useState({ enabled: false, success: false, message: "" });

    const dispatch = useDispatch();

    const getArbres = useSelector(state => state.arbres);
    const getTypes = useSelector(state => state.typesArbres);

    const { typesArbres = [], loading = true } = getTypes;
    const { arbres = [] } = getArbres;

    const Types = typesArbres.map((type) => ({ ...type, number: arbres.filter(arbre => arbre.type.id == type.id).length }));

    const handleAddType = async (libelle) => {

        if (!libelle) {
            setError({ enabled: true, success: false, message: 'Veuilez remplir le champ !' })
        }

        setPending(true);
        await TypesArbresProvider.create(libelle)
            .then((res) => {
                if (res.typearbre) {
                    setError({ enabled: true, success: true, message: 'Type enregistré avec succès !' })
                    setAddingType(false);
                    dispatch(getTypesArbresAction());
                }
            })
            .catch((e) => {
                console.log(e);
                setError({ enabled: true, success: false, message: "Une erreur est survenue durant l'enregistrement du type !" })
            })
        setPending(false);
    }

    return (
        <Container>


            <Modal visible={pending} transparent>
                <View style={styles.modalContent}>
                    <View style={[{ backgroundColor: colors.primary }, styles.loadingModal]}>
                        <StyledText style={{ color: colors.specialText, marginVertical: 5 }}>Enregistrement...</StyledText>
                        <ActivityIndicator size={30} color={colors.specialText} />
                    </View>
                </View>
            </Modal>

            <Modal visible={loading} transparent>
                <View style={styles.modalContent}>
                    <View style={[{ backgroundColor: colors.primary }, styles.loadingModal]}>
                        <StyledText style={{ color: colors.specialText, marginVertical: 5 }}>Chargement...</StyledText>
                        <ActivityIndicator size={30} color={colors.specialText} />
                    </View>
                </View>
            </Modal>


            <InputModal
                visible={addingType}
                label='Espece...'
                onRequestClose={() => { setAddingType(false) }}
                onSubmit={handleAddType}
            />

            <Header style={{ backgroundColor: colors.primary }}>
                <Left>
                    <TouchableOpacity
                        onPress={() => { navigation.goBack() }}
                        style={{ paddingLeft: 5, paddingRight: 10, paddingVertical: 7 }}
                    >
                        <Icon type='MaterialIcons' name='arrow-back' style={{ color: colors.specialText }} />
                    </TouchableOpacity>
                </Left>
                <Body style={{ flex: 2 }}>
                    <StyledText type='regular' style={{ color: colors.specialText, textTransform: 'uppercase', fontSize: 16 }}>Espèces d'arbres</StyledText>
                </Body>
                <Right></Right>
            </Header>

            <Content>
                {Types.map((type, index) => <TypeListCard type={type} navigation={navigation} key={index} />)}
            </Content>

            <Fab onPress={() => { setAddingType(true) }} style={{ backgroundColor: colors.primary }}>
                <Icon type='MaterialIcons' name='add' />
            </Fab>

        </Container>
    )
}

export default TypesScreen

const styles = StyleSheet.create({
    modalContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(1, 1, 1, .5)'
    },
    loadingModal: {
        width: '50%',
        height: 100,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center'
    },
})
