import React, { useState } from 'react'
import { useFocusEffect, useTheme } from '@react-navigation/native'
import { StyleSheet, View, TouchableOpacity, Image, Modal, ActivityIndicator, Alert, ToastAndroid } from 'react-native'
import { Container, Header, Left, Body, Right, Icon, Content, Form, Switch } from 'native-base'


import StyledInput from '../../components/StyledInput'
import StyledText from '../../components/StyledText'
import { AuthProvider } from '../../providers/Auth'
import { useSelector } from 'react-redux'


const UpdateProfileScreen = ({ navigation }) => {

    const { colors } = useTheme();

    const [loading, setLoading] = useState(false);

    const getCurrentUser = useSelector(state => state.currentUser);
    const [currentUser, setCurrentUser] = useState(getCurrentUser.currentUser);

    const [displayName, setDisplayName] = useState(currentUser.nom);
    const [phoneNumber, setPhoneNumber] = useState(currentUser.telephone);
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');

    const [updatePassword, setUpdatePassword] = useState(false);

    const [error, setError] = useState({ enabled: false, success: false, message: '' });

    const HandleUpdateProfile = async () => {

        if (!(currentUser.nom != displayName || updatePassword || currentUser.telephone != phoneNumber)) {
            ToastAndroid.showWithGravity('Pas de modifications', ToastAndroid.LONG, ToastAndroid.CENTER);
            return false;
        }

        if (currentUser.nom != displayName || currentUser.telephone != phoneNumber) {
            setLoading(true);
            const res = await AuthProvider.update(currentUser.id, displayName, currentUser.email, phoneNumber, currentUser.email, currentUser.password);
            if (res.user) {
                setCurrentUser(res.user);
                setError({ enabled: true, success: true, message: 'Votre profil a été mis à jour avec succès !' });
            }
        }

        if (updatePassword) {
            if (oldPassword != currentUser.password) {
                setError({ enabled: true, message: "L'ancien mot de passe ne correspond pas !" });
            } else if (newPassword.length < 6) {
                setError({ enabled: true, message: 'Le mot de passe doit comporter au moins 6 caractères !' });
            } else {
                Alert.alert('INFORMATIONS', 'Confirmez-vous la modification du mot de passe ?', [
                    {
                        text: "Annuler",
                        style: 'cancel'
                    },
                    {
                        text: 'Confirmer',
                        onPress: async () => {
                            const res = await AuthProvider.update(currentUser.id, displayName, currentUser.email, phoneNumber, currentUser.email, currentUser.password);
                            if (res.user) setCurrentUser(res.user);
                        }
                    }
                ])
            }

        }

        setLoading(false);
    }


    // useFocusEffect(React.useCallback(() => {

    //     AuthProvider.get(currentUser.id)
    //         .then((user) => {
    //             setCurrentUser(user);
    //         })
    //         .catch((e) => {
    //             console.log(e);
    //         })

    //     return () => {

    //     }
    // }, []))

    return (
        <Container style={{ backgroundColor: colors.specialText }}>

            <Modal visible={loading} transparent>
                <View style={styles.modalContent}>
                    <View style={[{ backgroundColor: colors.primary }, styles.loadingModal]}>
                        <StyledText style={{ color: colors.specialText, marginVertical: 5 }}>Chargement...</StyledText>
                        <ActivityIndicator size={30} color={colors.specialText} />
                    </View>
                </View>
            </Modal>

            <Modal visible={error.enabled} transparent onRequestClose={() => { setError({ enabled: false, message: '' }) }}>
                <View style={styles.modalContent}>
                    <View style={[{ backgroundColor: error.success ? colors.success : colors.warning }, styles.errorModal]}>
                        <TouchableOpacity
                            style={styles.modalCloseButton}
                            onPress={() => { setError({ enabled: false, message: '' }) }}
                        >
                            <StyledText style={{ color: colors.specialText }}>X</StyledText>
                        </TouchableOpacity>
                        <StyledText style={{ color: colors.specialText, marginVertical: 5 }}>{error.message}</StyledText>
                    </View>
                </View>
            </Modal>


            <Header style={{ backgroundColor: colors.primary }}>
                <Left>
                    <TouchableOpacity
                        onPress={() => { navigation.openDrawer() }}
                        style={styles.backButton}
                    >
                        <Icon type='MaterialIcons' name='apps' style={{ color: colors.specialText }} />
                    </TouchableOpacity>
                </Left>
                <Body>
                    <StyledText type='regular' style={{ color: colors.specialText, textTransform: 'uppercase', fontSize: 14 }}>Mise a jour</StyledText>
                </Body>
                <Right>
                    <TouchableOpacity
                        activeOpacity={.3}
                        onPress={HandleUpdateProfile}
                        style={[styles.checkButton]}>
                        <Icon type='MaterialIcons' name="check" style={{ color: colors.specialText }} />
                    </TouchableOpacity>
                </Right>
            </Header>

            <Content>
                <View style={styles.imageEditing}>
                    <Image
                        source={require('./../../assets/images/default.png')}
                        style={styles.image}
                    />
                </View>

                <View style={{ flex: 1, paddingHorizontal: 10 }}>
                    <Form>

                        <StyledInput
                            label='Votre nom'
                            defaultValue={currentUser.nom}
                            onChangeText={(text) => { setDisplayName(text); }}
                        />

                        <StyledInput
                            props={{ keyboardType: 'numeric', textContentType: 'telephoneNumber', dataDetectorTypes: 'phoneNumber' }}
                            label='Votre numéro de téléphone'
                            defaultValue={currentUser.telephone}
                            onChangeText={(text) => { setPhoneNumber(text); }}
                        />

                        <View style={{ marginTop: 30 }}>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <StyledText type='medium'>Modifier le mot de passe</StyledText>
                                <Switch value={updatePassword} onValueChange={setUpdatePassword} />
                            </View>

                            <View style={{ opacity: updatePassword ? 1 : .4 }}>
                                <StyledInput
                                    label='Ancien Mot de passe'
                                    defaultValue='abcdefgijkl'
                                    password
                                    onChangeText={setOldPassword}
                                    disabled={!updatePassword}
                                />
                                <StyledInput
                                    label='Nouveau Mot de passe'
                                    defaultValue='abcdefgijkl'
                                    password
                                    onChangeText={setNewPassword}
                                    disabled={!updatePassword}
                                />
                            </View>

                        </View>

                        <View style={{ marginVertical: 20 }}></View>
                    </Form>
                </View>


            </Content>

        </Container>
    )
}

export default UpdateProfileScreen

const styles = StyleSheet.create({
    modalContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(1, 1, 1, .5)'
    },
    loadingModal: {
        width: '50%',
        height: 100,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalCloseButton: {
        paddingHorizontal: 10,
        position: 'absolute',
        top: 7,
        right: 5
    },
    errorModal: {
        width: '70%',
        height: 90,
        paddingHorizontal: 15,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalGrid: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        width: '100%'
    },
    backButton: {
        paddingLeft: 5,
        paddingRight: 10,
        paddingVertical: 7
    },
    checkButton: {
        paddingRight: 8,
        paddingLeft: 30,
        paddingVertical: 12
    },
    imageEditing: {
        alignItems: 'center',
        marginVertical: 25
    },
    image: {
        width: 120,
        height: 120,
        borderRadius: 60
    },
    cameraButton: {
        position: 'absolute',
        right: '27%',
        top: '60%',
        paddingVertical: 20,
        width: 50,
        height: 50
    }
})
