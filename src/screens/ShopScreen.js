import React, { useState } from 'react'
import { PixelRatio, StyleSheet, Text, View, ScrollView, Image, TouchableOpacity } from 'react-native'
import { FlatListSlider } from '../helpers/flatlist-slider';

import ImageView from 'react-native-image-viewing';
import { useTheme } from '@react-navigation/native';
import { TextInput } from 'react-native-gesture-handler';
import { Badge, Button, Icon } from 'native-base';
import AnimatedHeader from '../helpers/animated-header/AnimatedHeader';
import StyledText from '../components/StyledText';
import PopularCategoryCard from '../components/Shop/PopularCategoryCard';
import ShopItemListCard from '../components/Shop/ShopItemListCard';
import CategoryCard from '../components/Shop/CategoryCard';
import CategoryCardIcon from '../components/Shop/CategoryCardIcon';
import { useSelector } from 'react-redux';

const ShopScreen = ({ navigation }) => {

    const { colors } = useTheme();

    const getProducts = useSelector(state => state.products);
    const { loading = true, products = [] } = getProducts;

    const getCart = useSelector(state => state.cart);
    const { cart = [] } = getCart;

    const Articles = products.filter(product => product.promotion && product.promotion.enabled == true);

    const MostArticles = products.sort((a, b) => b.sold - a.sold).slice(0, 4);

    const CategoriesIcons1 = [
        {
            icon: 'electrical-services',
            title: "Electricite"
        },
        {
            icon: 'sports-hockey',
            title: "Fixation"
        },
        {
            icon: 'square-foot',
            title: "Outillage"
        },
    ];

    const CategoriesIcons2 = [
        {
            icon: 'plumbing',
            title: "Plomberie"
        },
        {
            icon: 'panorama-photosphere',
            title: "Tuyauterie"
        },
        {
            icon: 'handyman',
            title: "Visserie"
        },
    ];


    return (
        <AnimatedHeader
            style={styles.main}
            title='Walners Store'
            renderLeft={() => (
                <TouchableOpacity
                    style={{ paddingRight: 20, paddingVertical: 7 }}
                    onPress={() => {
                        navigation.openDrawer();
                    }}
                >
                    <Icon type='MaterialIcons' name='apps' style={{ marginLeft: 20, color: colors.specialText }} />
                </TouchableOpacity>
            )}
            renderRight={() => (
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                        onPress={() => { navigation.push('CartScreen'); }}
                    >

                        <Icon type='MaterialIcons' name='shopping-cart' style={{ marginRight: 20, color: colors.specialText }} />
                        {getCart.cart.filter(item => item.unread == true).length != 0 && (
                            <Badge style={{ position: 'absolute', top: -5, right: 5, width: 25, height: 25 }}>
                                <StyledText style={{ color: colors.specialText, fontSize: 12 }}>{getCart.cart.filter(item => item.unread == true).length}</StyledText>
                            </Badge>
                        )}
                    </TouchableOpacity>
                    <Icon type='MaterialIcons' name='notifications' style={{ marginRight: 20, color: colors.specialText }} />
                </View>
            )}
            backStyle={{ marginLeft: 10 }}
            titleStyle={[styles.headerTitle, { color: colors.specialText }]}
            headerMaxHeight={160}
            imageSource={require('./../assets/images/3.jpg')}
            toolbarColor={colors.primary}
            disabled={false}
            imageStyle={styles.headerImage}
        >
            <ScrollView>
                <View style={styles.content}>

                    <View style={styles.searchSection}>
                        <TextInput keyboardType='web-search' placeholder='Rechercher un produit' style={[styles.textInput, { borderColor: colors.light }]} />
                        <Icon type='MaterialIcons' name='search' style={styles.searchIcon} />
                    </View>

                    <View style={styles.mainSection}>
                        <View style={styles.categoriesHeader}>
                            <StyledText type='medium' style={styles.topCategories}>Categories disponibles</StyledText>
                        </View>

                        <View style={styles.categoriesRow}>
                            {CategoriesIcons1.map((categorie, index) => <CategoryCardIcon navigation={navigation} key={index} category={categorie} />)}
                        </View>

                        <View style={styles.categoriesRow}>
                            {CategoriesIcons2.map((categorie, index) => <CategoryCardIcon navigation={navigation} key={index} category={categorie} />)}
                        </View>

                        <View style={[styles.productsSection, styles.firstProductsSection]}>
                            <View>
                                <StyledText type='medium' style={styles.productSectionTitle}>Produits en soldes</StyledText>
                            </View>

                            <ScrollView showsHorizontalScrollIndicator={false} horizontal style={styles.scrollArticles}>
                                {Articles.map((item, index) => <ShopItemListCard navigation={navigation} key={index} item={item} />)}
                            </ScrollView>
                        </View>

                        <View style={styles.productsSection}>
                            <View>
                                <StyledText type='medium' style={styles.productSectionTitle}>Produits en vedette</StyledText>
                            </View>

                            <ScrollView showsHorizontalScrollIndicator={false} horizontal style={styles.scrollArticles}>
                                {MostArticles.map((item, index) => <ShopItemListCard navigation={navigation} key={index} item={item} />)}
                            </ScrollView>
                        </View>

                    </View>
                </View>
            </ScrollView>
        </AnimatedHeader>
    )
}

export default ShopScreen

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    headerTitle: {
        fontSize: 22,
        left: 20,
        bottom: 20,
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    headerImage: {
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
    },
    content: {
        marginHorizontal: 10,
        marginTop: 20
    },
    searchSection: {
        marginHorizontal: 10
    },
    textInput: {
        paddingHorizontal: 25,
        borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
        borderRadius: 25,
        height: 45,
        backgroundColor: "rgba(100, 100, 100, .1)",
    },
    searchIcon: {
        position: 'absolute',
        right: 12,
        top: '20%',
        opacity: .5
    },
    mainSection: {
        marginTop: 22
    },
    categoriesHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 12
    },
    topCategories: {
        fontSize: 18
    },
    seeMoreButton: {
        paddingLeft: 15,
        height: 35
    },
    seeMoreButtonText: {
        paddingRight: 0
    },
    categoriesRow: {
        flexDirection: 'row',
        marginTop: 10,
        justifyContent: 'space-evenly'
    },
    productsSection: {
        marginVertical: 15
    },
    firstProductsSection: {
        marginTop: 25,
        marginBottom: 0
    },
    productSectionTitle: {
        fontSize: 18
    },
    scrollArticles: {
        marginTop: 4
    }
})
