import React, { useState, Fragment, useRef } from 'react'
import { TabRouter, useFocusEffect, useTheme } from '@react-navigation/native'
import { Badge, Body, Button, Col, Container, Content, Grid, Header, Icon, Left, ListItem, Right, Row, View } from 'native-base'
import { BackHandler, Dimensions, ScrollView, StatusBar, StyleSheet, TextInput, TouchableOpacity } from 'react-native'

import StyledText from '../components/StyledText'

import * as Animatable from 'react-native-animatable';
import { useDispatch, useSelector } from 'react-redux'
import ArbreListCard from '../components/Arbres/ArbreListCard'

const HomeScreen = ({ route, navigation }) => {

    const { colors } = useTheme();

    const [settingActive, setSettingActive] = useState(false);
    const [settingOpen, setSettingOpen] = useState(false);
    const [showSettings, setshowSettings] = useState(false);

    const [ArbresToDisplay, setArbresToDisplay] = useState([]);
    const [TypesArbresToDisplay, setTypesArbresToDisplay] = useState([]);

    const [searchText, setSearchText] = useState('');

    const [screenDimensions, setScreenDimensions] = useState({ width: Dimensions.get('window').width, height: Dimensions.get('window').height });

    const inputRef = useRef(null);

    const OpenAnimation = {
        from: {
            borderBottomLeftRadius: 5000,
            borderTopLeftRadius: 5000,
            borderBottomRightRadius: 5000,
            width: 0,
            height: 0,
            right: 25,
            marginTop: 120
        },
        to: {
            borderBottomLeftRadius: 0,
            borderTopLeftRadius: 0,
            borderBottomRightRadius: 0,
            width: screenDimensions.height,
            height: screenDimensions.height,
            right: 0,
            marginTop: 0
        },
    };

    const CloseAnimation = {
        from: {
            borderBottomLeftRadius: 0,
            borderTopLeftRadius: 0,
            borderBottomRightRadius: 0,
            width: screenDimensions.height,
            height: screenDimensions.height,
            right: 0,
            marginTop: 0
        },
        to: {
            borderBottomLeftRadius: 5000,
            borderTopLeftRadius: 5000,
            borderBottomRightRadius: 5000,
            width: 0,
            height: 0,
            right: 25,
            marginTop: 120
        },
    }


    useFocusEffect(React.useCallback(() => {

        const backEvent = BackHandler.addEventListener('hardwareBackPress', () => {
            if (searchText) {
                setSearchText('');
                inputRef?.current.clear();
                return true;
            } else if (settingActive) {
                setSettingOpen(false);
                return true;
            }

            return false;
        });

        Dimensions.addEventListener('change', (newSizes) => {
            setScreenDimensions({ with: newSizes.window.width, height: newSizes.window.height });
        });

        return () => {
            backEvent.remove();
            Dimensions.removeEventListener('change');
        }
    }, [settingActive, searchText]));


    const dispatch = useDispatch();

    const getTypesArbres = useSelector(state => state.typesArbres)
    const { typesArbres = [] } = getTypesArbres;

    const [TypesFilter, setTypes] = useState(typesArbres.map(type => ({ ...type, selected: false })));

    const getArbres = useSelector(state => state.arbres)
    const { arbres = [] } = getArbres;

    const TopTypes = typesArbres
        .map((type) => ({ ...type, number: getArbres.arbres.filter(arbre => arbre.type.id == type.id).length }))
        .sort((a, b) => b.number - a.number)
        .slice(0, 4);

    const Arbres = arbres.slice(0, 3);

    const getLocationInfos = useSelector(state => state.locationInfos)
    const { locationInfos = { infos: { street: '', adminArea5: '', adminArea5: '' } } } = getLocationInfos;

    return (
        <Container>
            <StatusBar backgroundColor={colors.primary} />
            {settingOpen == false &&
                (<Header searchBar androidStatusBarColor={colors.primary} style={{ height: 100, backgroundColor: colors.primary }}>
                    <View style={{ flex: 1, paddingTop: 20 }}>
                        <StyledText type='regular' style={{ fontSize: 12, color: colors.specialText }}>Votre position</StyledText>
                        <StyledText type='bold' style={{ fontSize: 15, color: colors.specialText }}>{locationInfos.infos?.street}, {locationInfos.infos?.adminArea5}, {locationInfos.infos?.adminArea3}</StyledText>
                    </View>
                </Header>)
            }

            <Animatable.View
                animation={
                    settingOpen ?
                        {
                            from: {
                                bottom: -63
                            },
                            to: {
                                bottom: -20
                            }
                        }
                        :
                        {
                            from: {
                                bottom: 70
                            },
                            to: {
                                bottom: 25
                            }
                        }
                }
                style={[styles.searchContainer, { zIndex: 555555555 }]}>

                <TextInput
                    ref={inputRef}
                    onChangeText={(value) => {
                        setSearchText(value);

                        if (!settingOpen) {
                            setTypesArbresToDisplay(typesArbres.filter((type) => type.libelle.toLowerCase().indexOf(value.toLowerCase()) != -1))
                        } else {
                            setArbresToDisplay(arbres.filter((arbre) => arbre.nom.toLowerCase().indexOf(value.toLowerCase()) != -1))
                        }
                    }}
                    placeholder={!settingOpen ? "Recherchez un type d'arbre " : "Rechercher un arbre"}
                    style={{ paddingHorizontal: 40, fontSize: 12 }}
                />

                <Icon type='MaterialIcons' name='search' style={[styles.searchIcon, { left: 8 }]} />
                <TouchableOpacity
                    onPress={() => {
                        setSearchText('');
                        setArbresToDisplay([]);
                        if (settingActive) {
                            setSettingOpen(false);
                            setshowSettings(false);
                        } else {
                            setSettingActive(true);
                            setSettingOpen(true);
                        }
                    }}
                    style={[styles.searchIcon, { right: 0, paddingHorizontal: 10 }]}>
                    <Icon type='MaterialIcons' name='apps' />
                </TouchableOpacity>
            </Animatable.View>

            {(settingActive == true) && (
                <Animatable.View onAnimationEnd={() => {
                    setSettingActive(active => settingOpen ? active : false);
                    if (settingOpen) {
                        setshowSettings(true);
                    }
                }} animation={settingOpen ? OpenAnimation : CloseAnimation} duration={500} style={{ position: 'absolute', top: 0, right: 0, backgroundColor: searchText ? "#fefefe" : colors.primary, zIndex: 1, paddingTop: 60 }}>
                    <View style={{ marginTop: 30, flex: 1, alignSelf: 'flex-end', width: screenDimensions.width }}>
                        {(showSettings == true) && (
                            <Fragment>
                                {searchText.length != 0 &&
                                    <ScrollView style={{ flex: 1, marginHorizontal: 20 }}>
                                        {ArbresToDisplay.map((item, index) => <ArbreListCard arbre={item} navigation={navigation} key={index} />)}
                                        <View style={{ marginVertical: 200 }}></View>
                                    </ScrollView>
                                }
                            </Fragment>
                        )}
                    </View>
                </Animatable.View>
            )}

            <Content style={{ bottom: 10, marginHorizontal: 10 }}>

                {(searchText != null && searchText != '' && searchText != undefined) ?
                    <View>
                        {TypesArbresToDisplay.map(type => (
                            <ListItem key={type.id} icon
                                onPress={() => {
                                    // navigation.push('ArbresScreen', { type });
                                }}
                            >
                                <Left>
                                    <Button style={{ backgroundColor: colors.placeholder }}>
                                        <Icon active type='MaterialIcons' name={'park'} />
                                    </Button>
                                </Left>
                                <Body>
                                    <StyledText type='medium'>{type.libelle}</StyledText>
                                </Body>
                            </ListItem>
                        ))}
                    </View>
                    :
                    <Fragment>

                        <Grid style={styles.ArbresGrid}>

                            <Row style={{ alignItems: 'center' }}>
                                <Col>
                                    <StyledText style={{ fontSize: 16 }} type='medium'>Top categories</StyledText>
                                </Col>
                                <Col>
                                    <TouchableOpacity onPress={() => { navigation.navigate('TypesScreen') }} style={styles.seeMoreButton}>
                                        <StyledText style={styles.seeMoreButtonText} type='regular'>Voir plus</StyledText>
                                    </TouchableOpacity>
                                </Col>
                            </Row>

                            <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ paddingVertical: 6 }}>
                                {[...TopTypes].map((type, index) => (
                                    <Button key={index} rounded small style={{ margin: 2, backgroundColor: colors.placeholder }}
                                        onPress={() => { navigation.navigate('ArbresByTypeScreen', { type }) }}
                                    >
                                        <StyledText style={{ paddingHorizontal: 8, color: '#FFF' }}>{type.libelle}</StyledText>
                                    </Button>
                                ))}
                            </ScrollView>

                        </Grid>

                        <Grid style={{ marginTop: 15 }}>
                            <Row style={{ alignItems: 'center' }}>
                                <Col>
                                    <StyledText style={{ fontSize: 16 }} type='medium'>Top arbres</StyledText>
                                </Col>
                                <Col>
                                    <TouchableOpacity
                                        onPress={() => { navigation.navigate('ArbresScreen') }}
                                        style={styles.seeMoreButton}
                                    >
                                        <StyledText style={styles.seeMoreButtonText} type='regular'>Voir plus</StyledText>
                                    </TouchableOpacity>
                                </Col>
                            </Row>
                        </Grid>

                        <View style={styles.societyList}>
                            {Arbres.map((arbre, index) => <ArbreListCard arbre={arbre} navigation={navigation} key={index} />)}
                        </View>
                    </Fragment>
                }
            </Content>

        </Container>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    center: {
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row'
    },
    searchContainer: {
        marginHorizontal: 20,
        bottom: 27,
        backgroundColor: '#FFF',
        borderRadius: 5,
        elevation: 2
    },
    searchIcon: {
        opacity: .4,
        position: 'absolute',
        top: '20%'
    },
    sliderImage: {
        width: Dimensions.get('window').width - 40,
        marginRight: 12,
        height: 160
    },
    ArbresGrid: {
        paddingTop: 8,
        alignItems: 'center'
    },
    seeMoreButton: {
        paddingLeft: 40,
        paddingRight: 8,
        paddingVertical: 8
    },
    seeMoreButtonText: {
        textAlign: 'right',
        fontSize: 14,
        opacity: .5
    },
    societyList: {
        marginTop: 10
    },
});
