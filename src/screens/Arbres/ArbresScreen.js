import React from 'react'
import { useFocusEffect, useTheme } from '@react-navigation/native';
import { Body, Button, Container, Content, Fab, Header, Icon, Left, Right } from 'native-base';
import { BackHandler, StyleSheet, ToastAndroid, TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux';
import TypeListCard from '../../components/Types/TypeListCard';
import StyledText from '../../components/StyledText';
import ArbreListCard from '../../components/Arbres/ArbreListCard';

const ArbresScreen = ({ route, navigation }) => {

    const { colors } = useTheme();

    const getArbres = useSelector(state => state.arbres);
    const { arbres = [] } = getArbres;

    const Arbres = () => {
        if (route.params && route.params.type) {
            return arbres.filter(arbre => arbre.type.id == route.params.type.id);
        }

        return arbres;
    }

    return (
        <Container>
            <Header androidStatusBarColor={colors.primary} style={{ backgroundColor: colors.primary }}>
                <Left>
                    <TouchableOpacity
                        onPress={() => {
                            if (arbres.length != Arbres().length) {
                                navigation.goBack()
                            } else {
                                navigation.openDrawer()
                            }
                        }}
                        style={{ paddingLeft: 5, paddingRight: 10, paddingVertical: 7 }}
                    >
                        <Icon type='MaterialIcons' name={arbres.length != Arbres().length ? 'arrow-back' : 'apps'} style={{ color: colors.specialText }} />
                    </TouchableOpacity>
                </Left>
                <Body style={{ flex: arbres.length != Arbres().length ? 2 : 1 }}>
                    <StyledText type='regular' style={{ color: colors.specialText, textTransform: 'uppercase', fontSize: 14 }}>{(route.params && route.params.type) ? route.params.type.libelle : 'LES ARBRES'}</StyledText>
                </Body>
                <Right></Right>
            </Header>

            <Content padder>
                {Arbres().map((arbre, index) => <ArbreListCard arbre={arbre} navigation={navigation} key={index} />)}
            </Content>

            <Fab
                onPress={() => {
                    navigation.navigate('AddArbreScreen');
                }}
                style={{ backgroundColor: colors.primary }}
            >
                <Icon type='MaterialIcons' name='add' />
            </Fab>
        </Container>
    )
}

export default ArbresScreen

const styles = StyleSheet.create({})
