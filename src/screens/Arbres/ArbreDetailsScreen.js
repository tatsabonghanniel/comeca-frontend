import { useFocusEffect, useTheme } from '@react-navigation/native';
import { Button, Icon } from 'native-base';
import React, { useState } from 'react'
import { StyleSheet, View, Dimensions, ScrollView, PixelRatio, BackHandler } from 'react-native'
import StyledText from '../../components/StyledText';

import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';

const ArbreDetailsScreen = ({ route, navigation }) => {

    const { arbre } = route.params;

    console.log(arbre);

    const { colors } = useTheme();

    return (
        <View style={{ flex: 1 }}>
            <ScrollView>
                <FastImage style={{ width: Dimensions.get('window').width, height: 200, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1) }} source={{ uri: arbre.image, cache: FastImage.cacheControl.immutable }} />

                <View style={styles.backButtonView}>
                    <Button rounded light onPress={() => { navigation.goBack() }}>
                        <Icon type='MaterialIcons' name='arrow-back' style={styles.backIcon} />
                    </Button>
                </View>

                <View style={styles.content}>

                    <StyledText style={{ fontSize: 20 }}>{arbre.nom}</StyledText>
                    <StyledText type='medium' style={{ fontSize: 18 }}>{arbre.nom_scientifique}</StyledText>

                    <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                        <StyledText type='medium' style={{ fontSize: 16 }}>Type :</StyledText>
                        <StyledText style={{ fontSize: 16 }}>{arbre.type.libelle}</StyledText>
                    </View>


                    <Animatable.Text style={{ marginTop: 25, textAlign: 'justify' }} animation='zoomInRight' duration={500} delay={500}>
                        <StyledText type='medium' style={{ lineHeight: 18, fontSize: 15, color: 'rgba(0, 0, 0, .6)' }}>{arbre.description}</StyledText>
                    </Animatable.Text>



                    <View style={{ margin: 20 }}></View>
                </View>

            </ScrollView>

            <View style={{ marginBottom: 10, flexDirection: 'row', justifyContent: 'space-around' }}>
                <Button danger rounded style={{ paddingRight: 18 }}
                    onPress={() => {
                        navigation.navigate("AddExploitationScreen", { arbre });
                    }}
                >
                    <Icon type='MaterialIcons' name='nature-people' style={{ color: colors.specialText, fontSize: 20 }} />
                    <StyledText style={{ fontSize: 12, color: colors.specialText }}>Enregistrer une exploitation</StyledText>
                </Button>
            </View>
        </View>
    )
}

export default ArbreDetailsScreen

const styles = StyleSheet.create({
    content: {
        marginHorizontal: 12,
        marginTop: 15,
        flex: 1
    },
    backButtonView: {
        position: 'absolute',
        paddingVertical: 10,
        paddingHorizontal: 10
    },
    backIcon: {
        fontSize: 25,
        color: '#FFF'
    },
})
