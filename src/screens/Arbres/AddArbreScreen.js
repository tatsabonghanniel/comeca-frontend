import React, { useState } from 'react'
import { useTheme } from '@react-navigation/native'
import { Container, Header, Left, Body, Right, Icon, Content, Button, Form, ListItem, Title, Fab } from 'native-base'
import { StyleSheet, Text, View, TouchableOpacity, Image, Platform, Modal, ActivityIndicator, ScrollView } from 'react-native'
import StyledText from '../../components/StyledText'

import storage from '@react-native-firebase/storage';

import ImageView from 'react-native-image-viewing'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker'
import StyledInput from '../../components/StyledInput'
import { ArbreProvider } from '../../providers/Arbres'
import { useDispatch, useSelector } from 'react-redux'
import { ToastAndroid } from 'react-native'
import InputModal from '../../helpers/InputModal'
import { TypesArbresProvider } from '../../providers/TypesArbres'
import { getArbresAction } from '../../redux/actions/Arbres'
import { getTypesArbresAction } from '../../redux/actions/TypesArbres'

const AddArbreScreen = ({ navigation }) => {

    const { colors } = useTheme();

    const [imageVisible, setImageVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);

    const [imageArbre, setImageArbre] = useState({});
    const [nomCommun, setNomCommun] = useState('');
    const [nomScientifique, setNomScientifique] = useState('');
    const [description, setDescription] = useState('');

    const dispatch = useDispatch();

    const getTypesArbres = useSelector(state => state.typesArbres)
    const { typesArbres = [] } = getTypesArbres;

    const [TypesArbresToDisplay, setTypesArbresToDisplay] = useState(typesArbres);
    const [idtype, setIdtype] = useState(typesArbres[0].id);

    const [selectedType, setSelectedType] = useState({ ...typesArbres[0] });

    const [error, setError] = useState({ enabled: false, success: false, message: '' });

    const [selecting, setSelecting] = useState(false);

    const [addingType, setAddingType] = useState(false);
    const [newType, setNewType] = useState('');

    const uploadImage = async (image) => {

        const { uri } = image;

        const time = new Date();

        const filename = `Images/Arbres/${time.getTime()}.jpg`;
        const uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;

        const task = storage()
            .ref(filename)
            .putFile(uploadUri);

        try {
            await task;
        } catch (e) {
            console.error(e);
        }

        return storage().ref(filename).getDownloadURL();

    };

    const PickImage = async (response) => {
        console.log(response);
        if (response.didCancel) {
            console.log('User cancelled image picker');
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
        } else {
            const source = { uri: response.assets[0].uri };
            setImageArbre(source);
        }
    }

    const getImageCamera = () => {
        launchCamera({
            mediaType: 'photo',
            saveToPhotos: true
        }, PickImage);
        setModalVisible(false);
    }

    const getImageLibrary = () => {
        launchImageLibrary({
            mediaType: 'photo',
        }, PickImage);
        setModalVisible(false);
    }

    const handleAddTree = async () => {

        if (!nomCommun || !nomScientifique || !description || !idtype) {
            setError({ enabled: true, success: false, message: "Veuillez remplir tous les champs !" });
            return false;
        }

        if (!imageArbre.uri) {
            setError({ enabled: true, success: false, message: "Veuillez ajouter une image !" });
            return false;
        }

        setLoading(true);
        await uploadImage(imageArbre)
            .then(async (imageUrl) => {
                await ArbreProvider.create(nomCommun, nomScientifique, description, imageUrl, idtype)
                    .then(() => {
                        ToastAndroid.showWithGravity("Arbre enregistré ave succès !", ToastAndroid.LONG, ToastAndroid.CENTER);
                        dispatch(getArbresAction());
                        navigation.navigate('ArbresScreen');
                    })
                    .catch((reason) => {
                        console.log(reason);
                        setError({ enabled: true, success: false, message: "Erreur lors de l'enregistrement de l'image !" });
                    })
            })
            .catch((e) => {
                console.log(e);
                setError({ enabled: true, success: false, message: "Erreur lors de l'importation de l'image !" });
            })

        setLoading(false);
    }

    const handleAddType = async (libelle) => {

        if (!libelle) {
            setError({ enabled: true, success: false, message: 'Veuilez remplir le champ !' })
        }

        setLoading(true);
        await TypesArbresProvider.create(libelle)
            .then((res) => {
                if (res.typearbre) {
                    setSelectedType(res.typearbre);
                    setIdtype(res.typearbre.id);
                    setError({ enabled: true, success: true, message: 'Type enregistré avec succès !' })
                    setAddingType(false);
                    setSelecting(false);
                    dispatch(getTypesArbresAction());
                }
            })
            .catch((e) => {
                console.log(e);
                setError({ enabled: true, success: false, message: "Une erreur est survenue durant l'enregistrement du type !" })
            })
        setLoading(false);
    }

    return (
        <Container style={{ backgroundColor: colors.specialText }}>

            <Modal visible={loading} transparent>
                <View style={styles.modalContent}>
                    <View style={[{ backgroundColor: colors.primary }, styles.loadingModal]}>
                        <StyledText style={{ color: colors.specialText, marginVertical: 5 }}>Enregistrement...</StyledText>
                        <ActivityIndicator size={30} color={colors.specialText} />
                    </View>
                </View>
            </Modal>

            <Modal visible={modalVisible} transparent onRequestClose={() => { setModalVisible(false) }}>
                <View style={styles.modalContent}>
                    <View style={[{ backgroundColor: colors.primary }, styles.errorModal]}>
                        <TouchableOpacity
                            style={styles.modalCloseButton}
                            onPress={() => { setModalVisible(false); }}
                        >
                            <StyledText style={{ color: colors.specialText }}>X</StyledText>
                        </TouchableOpacity>
                        <View style={styles.modalGrid}>

                            <TouchableOpacity onPress={getImageCamera} style={{ alignItems: 'center' }}>
                                <Icon type='MaterialIcons' name='photo-camera' style={{ fontSize: 45, color: colors.specialText }} />
                                <StyledText type='medium' style={{ color: colors.specialText }}>Camera</StyledText>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={getImageLibrary} style={{ alignItems: 'center' }}>
                                <Icon type='MaterialIcons' name='photo' style={{ fontSize: 45, color: colors.specialText }} />
                                <StyledText type='medium' style={{ color: colors.specialText }}>Galerie</StyledText>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </Modal>

            <Modal visible={error.enabled} transparent onRequestClose={() => { setError({ enabled: false, message: '' }) }}>
                <View style={styles.modalContent}>
                    <View style={[{ backgroundColor: error.success ? colors.success : colors.warning }, styles.errorModal]}>
                        <TouchableOpacity
                            style={styles.modalCloseButton}
                            onPress={() => { setError({ enabled: false, message: '' }) }}
                        >
                            <StyledText style={{ color: colors.specialText }}>X</StyledText>
                        </TouchableOpacity>
                        <StyledText style={{ color: colors.specialText, marginVertical: 5 }}>{error.message}</StyledText>
                    </View>
                </View>
            </Modal>


            <Modal visible={selecting} onRequestClose={() => { setSelecting(false) }}>
                <InputModal
                    visible={addingType}
                    label='Espece...'
                    onRequestClose={() => { setAddingType(false) }}
                    onSubmit={handleAddType}
                />
                <Header style={{ backgroundColor: colors.primary }}>
                    <Left>
                        <TouchableOpacity
                            onPress={() => { setSelecting(false) }}
                            style={styles.backButton}
                        >
                            <Icon type='MaterialIcons' name='arrow-back' style={{ color: colors.specialText }} />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ flex: 3 }}>
                        <StyledText style={{ fontSize: 18, color: colors.specialText }}>Choix de l'espèce de l'arbre</StyledText>
                    </Body>
                </Header>
                <ScrollView>
                    {TypesArbresToDisplay.map(type => (
                        <ListItem key={type.id} icon
                            onPress={() => {
                                setIdtype(type.id);
                                setSelectedType(type);
                                setSelecting(false);
                            }}
                        >
                            <Left>
                                <Button style={{ backgroundColor: colors.placeholder }}>
                                    <Icon active type='MaterialIcons' name={'park'} />
                                </Button>
                            </Left>
                            <Body>
                                <StyledText type='medium'>{type.libelle}</StyledText>
                            </Body>
                        </ListItem>
                    ))}
                </ScrollView>
                <Fab
                    onPress={() => { setAddingType(true) }}
                    style={{ backgroundColor: colors.placeholder }}
                >
                    <Icon type='MaterialIcons' name='add' />
                </Fab>
            </Modal>


            <Header style={{ backgroundColor: colors.primary }}>
                <Left>
                    <TouchableOpacity
                        onPress={() => { navigation.goBack() }}
                        style={styles.backButton}
                    >
                        <Icon type='MaterialIcons' name='arrow-back' style={{ color: colors.specialText }} />
                    </TouchableOpacity>
                </Left>
                <Body style={{ flex: 2 }}>
                    <StyledText type='regular' style={{ color: colors.specialText, textTransform: 'uppercase', fontSize: 16 }}>Ajouter un arbre</StyledText>
                </Body>
                <Right>
                    <TouchableOpacity
                        activeOpacity={.3}
                        onPress={handleAddTree}
                        style={[styles.checkButton]}>
                        <Icon type='MaterialIcons' name="check" style={{ color: colors.specialText }} />
                    </TouchableOpacity>
                </Right>
            </Header>

            <Content>
                <View style={styles.imageEditing}>

                    <ImageView
                        visible={imageVisible}
                        images={[imageArbre.uri ? imageArbre : require('./../../assets/images/svg/1.png')]}
                        onRequestClose={() => { setImageVisible(false) }}
                    />

                    <TouchableOpacity
                        onPress={() => { setImageVisible(true) }}
                    >
                        <Image
                            source={imageArbre.uri ? imageArbre : require('./../../assets/images/svg/1.png')}
                            style={styles.image}
                        />
                    </TouchableOpacity>
                    <Button icon rounded small onPress={() => { setModalVisible(true) }} style={[styles.cameraButton, { backgroundColor: colors.placeholder }]}>
                        <Icon type='MaterialIcons' name='photo-camera' style={{ fontSize: 20 }} />
                    </Button>
                </View>

                <View style={{ flex: 1, paddingRight: 12 }}>
                    <Form>

                        <View>
                            <StyledInput
                                label="Espèce de l'arbre"
                                defaultValue={selectedType.libelle}
                                updatedValue={selectedType.libelle}
                                disabled
                            />
                            <TouchableOpacity
                                style={{ position: 'absolute', height: '110%', width: '100%' }}
                                onPress={() => { setSelecting(true) }}
                            >
                            </TouchableOpacity>
                        </View>

                        <StyledInput
                            label='Nom commun'
                            onChangeText={(text) => { setNomCommun(text); }}
                        />

                        <StyledInput
                            label='Nom Scientifique'
                            onChangeText={(text) => { setNomScientifique(text); }}
                        />

                        <View style={{ marginTop: 30 }}>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                <StyledText type='medium'>Ajouter une description</StyledText>
                            </View>

                            <StyledInput
                                label='Description'
                                onChangeText={(text) => { setDescription(text); }}
                                textarea
                            />

                        </View>

                        <View style={{ marginVertical: 20 }}></View>
                    </Form>
                </View>


            </Content>

        </Container>
    )
}

export default AddArbreScreen

const styles = StyleSheet.create({
    modalContent: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(1, 1, 1, .5)'
    },
    loadingModal: {
        width: '50%',
        height: 100,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalCloseButton: {
        paddingHorizontal: 10,
        position: 'absolute',
        top: 7,
        right: 5
    },
    errorModal: {
        width: '70%',
        height: 90,
        paddingHorizontal: 15,
        borderRadius: 13,
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalGrid: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        width: '100%'
    },
    backButton: {
        paddingLeft: 5,
        paddingRight: 10,
        paddingVertical: 7
    },
    checkButton: {
        paddingRight: 8,
        paddingLeft: 30,
        paddingVertical: 12
    },
    imageEditing: {
        alignItems: 'center', marginVertical: 25
    },
    image: {
        width: 120, height: 120, borderRadius: 60
    },
    cameraButton: {
        position: 'absolute',
        right: '27%',
        top: '60%',
        paddingVertical: 20,
        width: 50,
        height: 50
    }
})
