import React, { useState } from 'react'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { View, ActivityIndicator } from 'react-native';


import Tabbar from '../components/Tabbar';
import StyledText from '../components/StyledText';

import HomeScreen from '../screens/HomeScreen';
import UpdateProfileScreen from '../screens/Settings/UpdateProfileScreen';
import AddExploitationScreen from '../screens/Exploitation/AddExploitationScreen';

import { getExploitationsAction } from '../redux/actions/Exploitations';
import { getArbresAction } from '../redux/actions/Arbres';
import { getTypesArbresAction } from '../redux/actions/TypesArbres';
import { getLocationInfosAction } from '../redux/actions/LocationInfos';
import { Button, Icon } from 'native-base';
import ArbresScreen from '../screens/Arbres/ArbresScreen';
import ArbreDetailsScreen from '../screens/Arbres/ArbreDetailsScreen';
import ExploitationsScreen from '../screens/Exploitation/ExploitationsScreen';
import ExploitationDetailsScreen from '../screens/Exploitation/ExploitationDetailsScreen';
import AddArbreScreen from '../screens/Arbres/AddArbreScreen';
import TypesScreen from '../screens/Types/TypesScreen';

const Tab = createBottomTabNavigator();

const HomeStack = createStackNavigator();
const ArbreStack = createStackNavigator();
const ExploitationStack = createStackNavigator();

const HomeStackScreen = ({ navigation, route }) => {

    return (
        <HomeStack.Navigator headerMode="none">
            <HomeStack.Screen name="HomeScreen" component={HomeScreen} />
        </HomeStack.Navigator>
    )
};

const ArbreStackScreen = ({ navigation, route }) => {

    return (
        <ArbreStack.Navigator headerMode="none">
            <ArbreStack.Screen name="ArbresScreen" component={ArbresScreen} />
            <ArbreStack.Screen name="TypesScreen" component={TypesScreen} />
            <ArbreStack.Screen name="ArbresByTypeScreen" component={ArbresScreen} />
            <ArbreStack.Screen name="ArbreDetailsScreen" component={ArbreDetailsScreen} />
            <ArbreStack.Screen name="AddArbreScreen" component={AddArbreScreen} />
        </ArbreStack.Navigator>
    )
};


const ExploitationStackScreen = ({ navigation, route }) => {

    return (
        <ExploitationStack.Navigator headerMode="none">
            <ExploitationStack.Screen name="ExploitationsScreen" component={ExploitationsScreen} />
            <ExploitationStack.Screen name="ExploitationDetailsScreen" component={ExploitationDetailsScreen} />
            <ArbreStack.Screen name="AddExploitationScreen" component={AddExploitationScreen} />
        </ExploitationStack.Navigator>
    )
};

const MainTabNavigation = () => {

    const dispatch = useDispatch();
    const getExploitations = useSelector(state => state.exploitations);
    const getTypesArbres = useSelector(state => state.typesArbres);
    const getArbres = useSelector(state => state.arbres);
    const { loading = true } = getExploitations;

    const [initializing, setInitializing] = useState(true);

    useEffect(() => {

        dispatch(getArbresAction());
        dispatch(getTypesArbresAction());
        dispatch(getLocationInfosAction());
        dispatch(getExploitationsAction());

        return () => {

        }
    }, [dispatch]);

    if (getExploitations.error ||
        getTypesArbres.error ||
        getArbres.error) {
        return (
            <StyledText>Une erreur s'est produite !</StyledText>
        )
    }

    if (
        !(getExploitations.loading == undefined || getExploitations.loading) &&
        !(getTypesArbres.loading == undefined || getTypesArbres.loading) &&
        !(getArbres.loading == undefined || getArbres.loading) &&
        initializing) {
        setInitializing(false);
    }


    if (initializing) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <StyledText type='bold' style={{ color: '#FFF', marginHorizontal: 10, fontSize: 30 }}>TREE LOCATOR</StyledText>
                <ActivityIndicator size={35} color='#FFF' />

                {(getArbres.error || getTypesArbres.error || getExploitations.error) && (
                    <View>
                        <StyledText type='bold' style={{ color: '#FFF', marginHorizontal: 10, fontSize: 13 }}>Veuillez verifier votre connexion a internet...</StyledText>
                        <Button icon rounded>
                            <Icon name='refresh' style={{ fontSize: 25 }} />
                        </Button>
                    </View>
                )}
            </View>
        )
    }


    return (
        <Tab.Navigator lazy={false} sceneContainerStyle={{ backgroundColor: "rgb(250, 250, 250)" }} tabBar={props => <Tabbar {...props} />}>
            <Tab.Screen name="Home" component={HomeStackScreen} />
            <Tab.Screen name="Trees" component={ArbreStackScreen} />
            <Tab.Screen name="Exploitations" component={ExploitationStackScreen} />
            <Tab.Screen name="Settings" component={UpdateProfileScreen} />
        </Tab.Navigator>
    )
}

export default MainTabNavigation
