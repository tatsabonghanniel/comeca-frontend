import React, { useEffect, useState } from 'react'
import DrawerNavigator from './../navigation/Drawer'
import RegisterScreen from './../screens/Auth/RegisterScreen';
import { createStackNavigator } from '@react-navigation/stack'
import AuthScreen from './../screens/Auth/AuthScreen'

import LoginScreen from './../screens/Auth/LoginScreen'
import { useDispatch, useSelector } from 'react-redux';
import { getCurrentUserAction } from '../redux/actions/CurrentUser';
import { View, ActivityIndicator } from 'react-native';
import StyledText from '../components/StyledText';

const MainNavigation = () => {

    const dispatch = useDispatch();
    const getCurrentUser = useSelector(state => state.currentUser);
    const { loading = true, currentUser = {} } = getCurrentUser;


    useEffect(() => {
        dispatch(getCurrentUserAction());
    }, [dispatch]);

    const AuthStack = createStackNavigator();

    const AuthStackScreen = () => (
        <AuthStack.Navigator headerMode="none">
            <AuthStack.Screen name="AuthScreen" component={AuthScreen} />
            <AuthStack.Screen name="RegisterScreen" component={RegisterScreen} />
            <AuthStack.Screen name="LoginScreen" component={LoginScreen} />
        </AuthStack.Navigator>
    );

    if (loading) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <StyledText type='bold' style={{ color: '#FFF', marginHorizontal: 10, fontSize: 30 }}>TREE LOCATOR</StyledText>
                <ActivityIndicator size={35} color='#FFF' />
            </View>
        )
    }

    return currentUser ? <DrawerNavigator /> : <AuthStackScreen />
};


export default MainNavigation;