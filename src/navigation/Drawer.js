/* eslint-disable react-native/sort-styles */
/* eslint-disable import/order */
import React from 'react';
import { Image, StyleSheet } from 'react-native';

import {
    DrawerItem,
    createDrawerNavigator,
    DrawerContentScrollView,
} from '@react-navigation/drawer';

import Animated from 'react-native-reanimated';

import LinearGradient from 'react-native-linear-gradient';

import { Icon, Button, Text } from 'native-base';
import { View } from 'react-native';

import MainTabNavigation from './MainTabNavigation';

import { Alert } from 'react-native';
import { AuthProvider } from '../providers/Auth';
import { useDispatch } from 'react-redux';
import { getCurrentUserAction } from '../redux/actions/CurrentUser';

const Drawer = createDrawerNavigator();

const Screens = ({ navigation, style }) => {
    return (
        <Animated.View style={StyleSheet.flatten([styles.stack, style])}>
            <MainTabNavigation />
        </Animated.View>
    );
};

const DrawerContent = props => {

    console.log(props.navigation.dangerouslyGetState());
    const dispatch = useDispatch();

    return (
        <DrawerContentScrollView {...props} scrollEnabled={false} contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
            <View style={{ width: '120%' }}>
                <DrawerItem
                    label="Accueil"
                    labelStyle={styles.drawerLabel}
                    onPress={() => props.navigation.navigate('Home')}
                    icon={() => <Icon type='MaterialIcons' name="home" style={{ color: '#FFF' }} size={16} />}
                    style={{ borderRadius: 15 }}
                />
                <DrawerItem

                    focused
                    activeBackgroundColor='#347850'
                    style={{ borderRadius: 15 }}

                    label="Arbres"
                    labelStyle={styles.drawerLabel}
                    onPress={() => props.navigation.navigate('Trees')}
                    icon={() => <Icon type='MaterialIcons' name="park" style={{ color: '#FFF' }} size={16} />}
                />
                <DrawerItem
                    label="Mes exploitations"
                    labelStyle={styles.drawerLabel}
                    onPress={() => props.navigation.navigate('Exploitations')}
                    icon={() => <Icon type='MaterialIcons' name="nature-people" style={{ color: '#FFF' }} size={16} />}
                />
                <DrawerItem
                    label="Mon profil"
                    labelStyle={styles.drawerLabel}
                    onPress={() => props.navigation.navigate('Settings')}
                    icon={() => <Icon type='MaterialIcons' name="person" style={{ color: '#FFF' }} size={16} />}
                />

                <DrawerItem
                    label="Deconnexion"
                    style={{ marginTop: 30 }}
                    labelStyle={styles.drawerLabel}
                    icon={() => <Icon type='MaterialIcons' name="logout" style={{ color: '#FFF' }} size={16} />}
                    onPress={() => {
                        Alert.alert('INFORMATIONS', "Voulez vraiment vous déconnecter ?", [
                            {
                                text: 'Annuler',
                                style: 'cancel'
                            },
                            {
                                text: 'Confirmer',
                                onPress: async () => {
                                    await AuthProvider.signOut();
                                    dispatch(getCurrentUserAction())
                                }
                            }
                        ]);
                    }}
                />
            </View>
        </DrawerContentScrollView>
    );
};

const DrawerNavigator = () => {
    const [progress, setProgress] = React.useState(new Animated.Value(0));
    const scale = Animated.interpolateNode(progress, {
        inputRange: [0, 1],
        outputRange: [1, 0.8],
    });
    const borderRadius = Animated.interpolateNode(progress, {
        inputRange: [0, 1],
        outputRange: [0, 16],
    });

    const animatedStyle = { borderRadius, transform: [{ scale }] };

    return (
        <LinearGradient style={{ flex: 1 }} colors={['#223e36', '#347850']}>
            <Drawer.Navigator
                // hideStatusBar
                drawerType="slide"
                overlayColor="transparent"
                drawerStyle={styles.drawerStyles}
                contentContainerStyle={{ flex: 1 }}
                drawerContentOptions={{
                    activeBackgroundColor: 'transparent',
                    activeTintColor: 'white',
                    inactiveTintColor: 'white',
                }}
                sceneContainerStyle={{ backgroundColor: 'transparent' }}
                drawerContent={props => {
                    setProgress(props.progress);
                    return <DrawerContent {...props} />;
                }}>
                <Drawer.Screen name="Screens">
                    {props => <Screens {...props} style={animatedStyle} />}
                </Drawer.Screen>

            </Drawer.Navigator>
        </LinearGradient>
    );
};

export default DrawerNavigator;

const styles = StyleSheet.create({
    stack: {
        flex: 1,
        shadowColor: '#FFF',
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.44,
        shadowRadius: 10.32,
        elevation: 5,
        // overflow: 'scroll',
        // borderWidth: 1,
    },
    drawerStyles: {
        flex: 1, width: '50%', backgroundColor: 'transparent'
    },
    drawerItem: {
        alignItems: 'flex-start', marginVertical: 0
    },
    drawerLabel: {
        color: 'white', marginLeft: -16,
        fontFamily: 'ProductSans-Medium',
        fontSize: 13
    },
    avatar: {
        width: 60,
        height: 60,
        borderRadius: 60,
        marginBottom: 16,
        borderColor: 'white',
        borderWidth: StyleSheet.hairlineWidth,
    },
});