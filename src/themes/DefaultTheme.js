export const currentTheme = {
    colors: {
        specialText: "#FFF",
        // primary: "#3F51B5",
        primary: "#223e36",
        placeholder: "#347850",
        placeholderBg: 'rgba(20, 100, 50, .1)',
        // secondary: "#35a163",
        success: "#5cb85c",
        ios: "#007aff",
        warning: "#f0ad4e",
        danger: "#d9534f",
        info: "#62B1F6"
    }
}