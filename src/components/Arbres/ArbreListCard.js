import React, { Fragment, useState } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { Card, CardItem, Body } from 'native-base'
import StyledText from '../StyledText'

const ArbreListCard = ({ arbre, navigation }) => {


    return (
        <Fragment>
            <Card style={{ flex: 0 }}>
                <CardItem button onPress={() => { navigation.navigate('ArbreDetailsScreen', { arbre }) }}>
                    <Body>
                        <Image source={{ uri: arbre.image }} style={{ height: 150, width: '100%', flex: 1, marginBottom: 10 }} />
                        <StyledText style={[styles.text, styles.nameText]}>{arbre.nom}</StyledText>
                        <StyledText type='regular' style={[styles.text, styles.scNameText]}>{arbre.nom_scientifique}</StyledText>
                    </Body>
                </CardItem>
            </Card>
        </Fragment>

    )
}

export default ArbreListCard

const styles = StyleSheet.create({
    text: {
        textAlign: 'center', width: '100%', color: '#000'
    },
    nameText: {
        fontSize: 18
    },
    scNameText: {
        fontSize: 16
    }
})
