import { useTheme } from '@react-navigation/native'
import { Body, Button, Icon, Left, ListItem } from 'native-base'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import StyledText from '../StyledText'

const TypeListCard = ({ type, navigation }) => {

    const { colors } = useTheme();

    return (
        <ListItem key={type.id} icon
            onPress={() => { navigation.push('ArbresByTypeScreen', { type }) }}
        >
            <Left>
                <Button style={{ backgroundColor: colors.placeholder }}>
                    <Icon active type='MaterialIcons' name={'park'} />
                </Button>
            </Left>
            <Body>
                <StyledText type='medium'>{type.libelle}</StyledText>
            </Body>
        </ListItem>
    )
}

export default TypeListCard

const styles = StyleSheet.create({})
