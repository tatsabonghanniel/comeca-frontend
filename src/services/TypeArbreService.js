import axios from 'axios';
import { API_HOST } from '../config/constants';

class TypeArbreService {

    async create(libelle) {

        let retour = {};

        await axios.post(`${API_HOST}/typesarbre`, { libelle })
            .then((res) => {
                retour = res.data;
            })
            .catch((reason) => {
                console.log(reason);
                retour = reason;
            })

        return retour;
    }

    async update(id, libelle) {
        let retour = {};

        axios.put(`${API_HOST}/typesarbre/${id}`, { libelle, id })
            .then((res) => {
                retour = res.data;
            })
            .catch((reason) => {
                console.log(reason);
                retour = reason;
            })

        return retour;
    }

}

export default TypeArbreService;