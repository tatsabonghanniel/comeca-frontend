import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { API_HOST } from '../config/constants';

class AuthService {

    async get(id) {
        let retour = {};

        await axios.get(`${API_HOST}/users/${id}`)
            .then((res) => {
                retour = res.data;
            })
            .catch((reason) => {
                console.log(reason);
                retour = reason;
            })

        return retour;
    }

    async register(nom, email, telephone, login, password) {

        let retour = {};

        await axios.post(`${API_HOST}/users`, { nom, email, telephone, login, password })
            .then(async (res) => {
                retour = res.data;
                if (retour.user) {
                    await AsyncStorage.setItem('@CURRENT_USER', JSON.stringify(retour.user));
                }
            })
            .catch((reason) => {
                console.log(reason);
                retour = reason;
            })

        return retour;
    }

    async update(id, nom, email, telephone, login, password) {

        let retour = {};

        await axios.put(`${API_HOST}/users/${id}`, { nom, email, telephone, login, password })
            .then(async (res) => {
                retour = res.data;
                if (retour.user) {
                    await AsyncStorage.setItem('@CURRENT_USER', JSON.stringify(retour.user));
                }
            })
            .catch((reason) => {
                console.log(reason);
                retour = reason;
            })

        return retour;
    }

    async login(login, password) {

        let retour = {};

        await axios.post(`${API_HOST}/users/login`, { login, password })
            .then(async (res) => {
                console.log(res.data);
                retour = res.data;
                if (retour.user) {
                    await AsyncStorage.setItem('@CURRENT_USER', JSON.stringify(retour.user));
                }
            })
            .catch((reason) => {
                console.log(reason);
                retour = reason;
            })

        return retour;
    }

    async signOut() {
        await AsyncStorage.removeItem('@CURRENT_USER');
    }

}

export default AuthService;