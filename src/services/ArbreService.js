import axios from 'axios';
import { API_HOST } from '../config/constants';

class ArbreService {

    async create(nom, nom_scientifique, description, image, idtype) {

        let retour = {};

        await axios.post(`${API_HOST}/arbres`, { nom, nom_scientifique, description, image, idtype })
            .then((res) => {
                retour = res.data;
            })
            .catch((reason) => {
                console.log(reason);
                retour = reason;
            })

        return retour;
    }

    async update(id, nom, nom_scientifique, description, image, idtype) {
        let retour = {};

        await axios.put(`${API_HOST}/arbres/${id}`, { nom, nom_scientifique, description, image, idtype, id })
            .then((res) => {
                retour = res.data;
            })
            .catch((reason) => {
                console.log(reason);
                retour = reason;
            })

        return retour;
    }

}

export default ArbreService;