import * as ActionsType from "../constants/Arbres";

export function ArbreReducer(state = [], action) {
    switch (action.type) {
        case ActionsType.GET_ARBRES_REQUEST:
            return {
                loading: true,
                arbres: [],
            }
        case ActionsType.GET_ARBRES_SUCCESS:
            return {
                loading: false,
                arbres: action.payload.arbres,
            }
        case ActionsType.GET_ARBRES_FAIL:
            return {
                loading: false,
                error: action.error
            }
        default:
            return state;
    }
}
