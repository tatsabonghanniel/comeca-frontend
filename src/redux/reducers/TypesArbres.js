import * as ActionsType from "../constants/TypesArbres";

export function TypesArbreReducer(state = [], action) {
    switch (action.type) {
        case ActionsType.GET_TYPESARBRES_REQUEST:
            return {
                loading: true,
                typesArbres: [],
            }
        case ActionsType.GET_TYPESARBRES_SUCCESS:
            return {
                loading: false,
                typesArbres: action.payload.typesArbres,
            }
        case ActionsType.GET_TYPESARBRES_FAIL:
            return {
                loading: false,
                error: action.error
            }
        default:
            return state;
    }
}
