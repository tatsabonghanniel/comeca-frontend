import { applyMiddleware, combineReducers, createStore } from "redux";
import * as ArbresReducers from "./reducers/Arbres";
import * as ExploitationsReducers from "./reducers/Exploitations";
import * as TypesArbresReducers from "./reducers/TypesArbres";
import * as CurrentUserReducers from "./reducers/CurrentUser";
import * as LocationInfosReducers from "./reducers/LocationInfos";

import thunk from 'redux-thunk';

export default createStore(
    combineReducers({
        arbres: ArbresReducers.ArbreReducer,
        exploitations: ExploitationsReducers.ExploitationReducer,
        typesArbres: TypesArbresReducers.TypesArbreReducer,
        currentUser: CurrentUserReducers.CurrentUserReducer,
        locationInfos: LocationInfosReducers.LocationInfosReducer
    }),
    applyMiddleware(thunk)
);