/* eslint-disable prettier/prettier */
import * as ActionsType from '../constants/Arbres';
import axios from 'axios';
import { API_HOST } from '../../config/constants';

export const getArbresAction = () => async dispatch => {

    dispatch({ type: ActionsType.GET_ARBRES_REQUEST });

    await axios.get(`${API_HOST}/arbres`)
        .then((res) => {
            dispatch({
                type: ActionsType.GET_ARBRES_SUCCESS,
                payload: { arbres: res.data.sort((a, b) => a.nom.toLowerCase().localeCompare(b.nom.toLowerCase())) },
            });
        })
        .catch((reason) => {
            dispatch({
                type: ActionsType.GET_ARBRES_FAIL,
                payload: { error: "Erreur lors de la recuperation des arbres" },
            });
        })
};
