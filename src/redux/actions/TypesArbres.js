/* eslint-disable prettier/prettier */
import * as ActionsType from '../constants/TypesArbres';
import axios from 'axios';
import { API_HOST } from '../../config/constants';

export const getTypesArbresAction = () => async dispatch => {

    dispatch({ type: ActionsType.GET_TYPESARBRES_REQUEST });

    await axios.get(`${API_HOST}/typesarbre`)
        .then((res) => {
            console.log(res);
            dispatch({
                type: ActionsType.GET_TYPESARBRES_SUCCESS,
                payload: { typesArbres: res.data.sort((a, b) => a.libelle.toLowerCase().localeCompare(b.libelle.toLowerCase())) },
            });
        })
        .catch((reason) => {
            dispatch({
                type: ActionsType.GET_TYPESARBRES_FAIL,
                payload: { error: "Erreur lors de la recuperation des exploitations" },
            });
        })
};
