import * as ActionsType from '../constants/CurrentUser';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { AuthProvider } from '../../providers/Auth';

export const getCurrentUserAction = () => async dispatch => {

    dispatch({ type: ActionsType.GET_CURRENTUSER_REQUEST });

    await AsyncStorage.getItem('@CURRENT_USER')
        .then(async (value) => {
            if (value) {
                const temp = JSON.parse(value);
                const user = await AuthProvider.get(temp.id);
                await AsyncStorage.setItem('@CURRENT_USER', JSON.stringify(user));
                dispatch({ type: ActionsType.GET_CURRENTUSER_SUCCESS, payload: { currentUser: user } });
            } else {
                dispatch({ type: ActionsType.GET_CURRENTUSER_SUCCESS, payload: { currentUser: null } });
            }
        })
        .catch((reason) => {
            dispatch({
                type: ActionsType.GET_CURRENTUSER_FAIL,
                payload: { error: "Erreur lors de la recuperation de l'user courant" },
            });
        })
};
