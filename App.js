import { NavigationContainer } from '@react-navigation/native'
import React from 'react'
import { currentTheme } from './src/themes/DefaultTheme'
import { Provider } from 'react-redux'
import store from './src/redux/store'
import MainNavigation from './src/navigation/MainNavigation'

const App = () => {

  return (
    <NavigationContainer theme={currentTheme}>
      <Provider store={store}>
        <MainNavigation />
      </Provider>
    </NavigationContainer>
  )
};


export default App;